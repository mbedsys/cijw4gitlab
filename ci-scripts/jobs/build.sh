citbx_use "dind"

job_main() {
    TARGET_CI_REGISTRY_IMAGE_TAG="$CI_REGISTRY_IMAGE:${CI_COMMIT_TAG:-${CI_COMMIT_REF_NAME##*/}}"
    # Build docker image
    TARGET_DOCKER_BUILD_ARGS+=(-t "$TARGET_CI_REGISTRY_IMAGE_TAG")
    docker build "${TARGET_DOCKER_BUILD_ARGS[@]}" .
    if [ -n "$CI_COMMIT_TAG" ] && [ -n "$CI_BUILD_TOKEN" ]; then
        docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
        docker push "$TARGET_CI_REGISTRY_IMAGE_TAG"
    fi
}

job_after() {
    local retcode=$1
    if [ $retcode -eq 0 ]; then
        print_info "Image \"$TARGET_CI_REGISTRY_IMAGE_TAG\" successfully generated"
    fi
}
