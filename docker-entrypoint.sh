#!/bin/bash -e
# cijw4gitlab: CI job wrapper for Gitlab
# Copyright (C) 2017-2018 ERCOM - Emeric Verschuur <emeric@mbedsys.org>
# Copyright (C) 2019-2021 MBEDSYS - Emeric Verschuur <emeric@mbedsys.org>
# 
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

print_critical() {
    printf "\033[91m[CI-Job-Wrapper::CRIT] %s\033[0m\n" "$@"
    exit 1
}

print_error() {
    printf "\033[91m[CI-Job-Wrapper::ERRO] %s\033[0m\n" "$@"
}

print_warning() {
    printf "\033[93m[CI-Job-Wrapper::WARN] %s\033[0m\n" "$@"
}

if [ "$CIJW_DEBUG" == "true" ]; then
    print_note() {
        printf "[CI-Job-Wrapper::NOTE] %s\n" "$@"
    }
else
    print_note() {
        return 0
    }
fi

print_info() {
    printf "\033[92m[CI-Job-Wrapper::INFO] %s\033[0m\n" "$@"
}

yaml2json() {
    cat "$@" | python -c 'import sys, yaml, json; json.dump(yaml.safe_load(sys.stdin), sys.stdout)'
}

cijw_query() {
    jq "$@" <<< "$CIJW_CONFIG_JSON"
}

cijw_process_image() {
    print_note "Processing image bloc..."
    eval "$( cijw_query -r '
        [[."'"$CI_JOB_NAME"'".image , .image][]
            | select(.!=null)]
            | if (. | length) != 0 then
                .[0] | if (. | type) == "string" then
                    @sh "CIJW_JOB_DOCKER_IMAGE=\(.)"
                elif (. | type) == "object" then
                    "CIJW_JOB_DOCKER_IMAGE=\(@sh "\(.name)")
                    CITBX_DOCKER_ENTRYPOINT=(\(.entrypoint | map(@sh "\(.)") | join(" ")))"
                else "" end
            else "" end'
    )"
    CIJW_DOCKER_JOB_COMMANDS=()
    if [ -n "${CITBX_DOCKER_ENTRYPOINT[0]}" ]; then
        CIJW_DOCKER_JOB_ARGS+=(--entrypoint "${CITBX_DOCKER_ENTRYPOINT[0]}")
        for cmd in "${CITBX_DOCKER_ENTRYPOINT[@]:1}"; do
            CIJW_DOCKER_JOB_COMMANDS+=("$cmd")
        done
    fi
    test -n "$CIJW_JOB_DOCKER_IMAGE" \
        || print_critical "Undefined image property for the job '$CI_JOB_NAME' into .ci-job-wrapper.yml"
    print_note "Image name: $CIJW_JOB_DOCKER_IMAGE"
}

cijw_process_network() {
    print_note "Processing network config..."
    eval "$(cijw_query -r '."'"$CI_JOB_NAME"'".docker_run_args.network 
        | if . | type == "string" then "CIJW_DOCKER_NETWORK=\(@sh"\(.)")" else "" end')"

    # Add notwork mode
    case "$CIJW_DOCKER_NETWORK" in
        '')
            if [ "$CIJW_DIND_MODE_ACTIVE" == "true" ]; then
                CIJW_DOCKER_NETWORK="host"
            else
                CIJW_DOCKER_NETWORK="container:$CIJW_DOCKER_ID"
            fi
            CIJW_DOCKER_JOB_ARGS+=(--network "$CIJW_DOCKER_NETWORK")
            ;;
        container:*|host)
            ;;
        *)
            CIJW_DOCKER_JOB_ARGS+=(--hostname "$HOSTNAME")
            CIJW_NETWORK_EDITABLE="true"
            ;;
    esac
}

cijw_process_docker_run_args() {
    print_note "Processing docker run args bloc..."
    eval "$( cijw_query -r '."'"$CI_JOB_NAME"'".docker_run_args
        | if (. | type) == "object" then . else {} end | to_entries
        | map({
            key: .key,
            value: (if (.value | type) == "array" then .value else [.value] end)
        }) | map(.key as $arg | .value | map(
            if (. | type) == "boolean" then
                "CIJW_DOCKER_JOB_ARGS+=(--\($arg))"
            elif (. | type) == "number" then
                "CIJW_DOCKER_JOB_ARGS+=(--\($arg) \(.))"
            else 
                "CIJW_DOCKER_JOB_ARGS+=(--\($arg) \(@sh"\(. | tostring)"))"
            end)
        ) | add // [] | .[]'
    )"
}

# hook executed on exit
cijw_exit_hook() {
    test -n "$CIJW_DOCKER_PREFIX" || print_critical "Assert: empty CIJW_DOCKER_PREFIX"
    for d in $(docker ps -a --filter "label=$CIJW_DOCKER_PREFIX" -q); do
        print_note "Cleanup docker $d..."
        docker rm -f $d > /dev/null 2>&1 || true
    done
    if [ -f /var/run/docker.pid ]; then
        print_note "Stop docker daemon $d..."
        # Kill the docker daemon
        local pid=$(cat /var/run/docker.pid)
        kill $pid 2> /dev/null || true
        wait $pid 2> /dev/null || true
    fi
    print_note "Remove docker folder $d..."
    cijw_rm_dind_dir() {
        for m in $(cat /proc/mounts | awk 'substr($2,0,'"${#CIJW_DIND_DIR}"') == "'"${CIJW_DIND_DIR}"'" {print $2}'); do
            umount -f $m || true
        done
        rm -rf $CIJW_DIND_DIR || true
    }
    cijw_rm_dind_dir
    while [ -d "$CIJW_DIND_DIR" ]; do
        sleep 1
        cijw_rm_dind_dir
    done
}

# Start a service
cijw_start_service() {
    local alias="$(sed -E 's/:[^:\/]+//g; s/[^a-zA-Z0-9\._-]/__/g' <<< "$1")"
    local network=$2
    local docker_args=(--name "$CIJW_DOCKER_PREFIX-$alias" --label "$CIJW_DOCKER_PREFIX")
    local ip
    shift 2
    if [ -z "$network" ]; then
        network=$CIJW_DOCKER_NETWORK
        docker_args+=(--network "$network")
    fi
    print_info "Starting service $alias..."
    docker run -d "${CIJW_DOCKER_COMMON_ARGS[@]}" "${docker_args[@]}" "$@"
    case "$network" in
        host|container:*)
            return 0
            ;;
        *)
            ;;
    esac
    case "$CIJW_DOCKER_NETWORK" in
        host|container:*)
            print_warning "Cannot add '--add-host' argument on the job container with network mode '$CIJW_DOCKER_NETWORK'"
            return 0
            ;;
        *)
            ;;
    esac
    ip=$(docker inspect $CIJW_DOCKER_PREFIX-$alias | jq -r .[0].NetworkSettings.Networks.bridge.IPAddress)
    if [ -z "$ip" ]; then
        print_critical "Unable to get the $alias service IP address" \
                        "This service seems to not be correctly started" \
                        "=> Try again with option \"$CI_JOB_NAME\".services.\"$alias\".docker_run_args.privileged=true"
    fi
    print_note "STARTED: $alias:$ip"
    CIJW_DOCKER_JOB_ARGS+=(--add-host "$alias:$ip")
}

# Start all services
cijw_start_services() {
    print_note "Processing services bloc..."
    eval "$(cijw_query -r '[.services // [], ."'"$CI_JOB_NAME"'".services] | add
        | map(if (. | type) == "string" then {name: ., alias: .} else . end)
        | map("cijw_start_service"
            + " \(@sh"\(.alias)")"
            + " \(@sh"\(.docker_run_args.network // "")")"
            + " \(.docker_run_args | if (. | type) == "object" then . else {} end | to_entries
                | map({key: .key, value: (if (.value | type) == "array" then .value else [.value] end)})
                | map(.key as $arg | .value | map(
                    if (. | type) == "boolean" then
                        "--\($arg)"
                    elif (. | type) == "number" then
                        "--\($arg) \(.)"
                    else 
                        "--\($arg) \(@sh"\(. | tostring)")"
                    end)
                ) | add // [] | join(" "))"
            + " \(.entrypoint | if (. | type) == "array" and (. | length) != 0 then
                "--entrypoint \(@sh"\(.[0])")"
                else "" end)"
            + " \(@sh"\(.name)") \(.command | if (. | type) == "array" then "\(@sh"\(.)")" else "" end)"
        )[] // ""'
    )"
}

# Check and create volumes if needed
cijw_add_volume() {
    local image=$1
    cnt_ref="ci_volume_$(sha256sum  <<< "$image" | cut -d\  -f 1)"
    print_note "Check for volume $cnt_ref..."
    remaining_try=${CIJW_VOL_CREATE_TRY_COUNT:-10}
    until [ "$(docker inspect $cnt_ref 2>/dev/null | jq -r '.[].State.Status')" == "created" ]; do
        print_info "Creating volume $cnt_ref from image $image..."
        # An error can occur when another wrapper try to create the same container
        # at the same time, and in this case, we can just ignore this error...
        if docker create --name $cnt_ref $image; then
            break
        elif ((--remaining_try)); then
            print_warning "Will retry to create the volume in a little while..."
            sleep ${CIJW_VOL_CREATE_TRY_INTVL:-10}
        else
            print_critical "Aborting due to too much errors"
        fi
    done
    print_note "Add volume: $cnt_ref"
    CIJW_DOCKER_JOB_ARGS+=(--volumes-from $cnt_ref)
}

# Call cijw_add_volume for each volumes
cijw_add_volumes() {
    print_note "Processing docker volumes bloc..."
    eval "$(cijw_query -r '."'"$CI_JOB_NAME"'"."volumes_from_images"
        | if . != null then . else [] end | map(@sh"cijw_add_volume \(.)")[]')"
    if [ "$CIJW_DIND_MODE_ACTIVE" == "true" ]; then
        local m moun_list=()
        while read -r m; do
            test -n "$m" || continue
            moun_list+=("$m")
        done <<< "$(echo "$CI_PROJECT_DIR:$CIJW_TARGET_VOLUMES" | tr ':' '\n' | sort -u)"
        for m in "${moun_list[@]}"; do
            print_note "Adding volume: $m"
            CIJW_DOCKER_JOB_ARGS+=(--volume "$m:$m")
        done
    else
        print_note "Add volume from the docker: $CIJW_DOCKER_ID"
        CIJW_DOCKER_JOB_ARGS+=(--volumes-from "$CIJW_DOCKER_ID")
    fi
}

# Detect if the environment is in interactive mode or not
if [ -t 1 ]; then
    print_note "Running in interactive mode"
    CIJW_DOCKER_JOB_ARGS+=(-ti)
else
    print_note "Running in non interactive mode"
    CIJW_DOCKER_JOB_ARGS+=(-i --attach=STDOUT --attach=STDERR)
fi

# Sanity checks
cd $CI_PROJECT_DIR
test -n "$CI" \
    || print_critical "This image must be started in a CI environment"
test -n "$CI_JOB_NAME" \
    || print_critical "Undefined variable CI_JOB_NAME !"

# Load the configuration
if [ -z "$CIJW_CONFIG_JSON" ]; then
    if [ -n "$CIJW_JOB_CONFIG_YAML" ]; then
        CIJW_CONFIG_JSON=$(echo -e "$CIJW_JOB_CONFIG_YAML" | yaml2json | jq -r '{"'"$CI_JOB_NAME"'":.}')
    elif [ -f ".ci-job-wrapper.yml" ]; then
        print_note "Read the file .ci-job-wrapper.yml"
        CIJW_CONFIG_JSON=$(yaml2json .ci-job-wrapper.yml)
    else
        print_critical "CI job wrapper configuration not found!" \
                        "You must:" \
                        "* Define the CIJW_JOB_CONFIG_YAML variable with the job configuration in YAML format" \
                        "* or define the job configuration in the .ci-job-wrapper.yml file"
    fi
fi

# Check job properties
eval "$(cijw_query -r '."'"$CI_JOB_NAME"'" | keys | map([.])
    | map(select(inside(["image", "volumes_from_images", "docker_run_args", "services"]) | not))
    | add | if . | length != 0 then
        @sh"print_critical \"Job '"$CI_JOB_NAME"': Unreconised (or no longer available) property list: \(.)\""
    else "" end')"

# Docker ID
CIJW_ID=$(head -c 8 /dev/urandom | od -t x8 -An | grep -oE '\w+')
CIJW_DOCKER_PREFIX="cijw-$CIJW_ID"

# Extract docker ID and add volume from this docker
CIJW_DOCKER_ID="$(cat /proc/self/cgroup | gawk 'match($0, /([a-f0-9]+)$/, ret) {print ret[1]; exit}')"
test -n "$CIJW_DOCKER_ID" \
    || print_critical "Unable to extract docker ID"

# Transmit CI* env variables to the new environment
for e in $(cat /proc/self/environ | tr -d '\n' | tr '\000' '\n' | cut -d '=' -f 1); do
    case "$e" in
        _|HOSTNAME|OLDPWD|PATH|PWD|SHLVL|HOME|USER)
            ;;
        *)
            CIJW_DOCKER_COMMON_ARGS+=(-e "$e")
            ;;
    esac
done

# copy DOCKER_AUTH_CONFIG into ~/.docker/config.json
if [ -n "$DOCKER_AUTH_CONFIG" ]; then
    mkdir -p ~/.docker/
    echo "$DOCKER_AUTH_CONFIG" > ~/.docker/config.json
fi

if [[ -v DOCKER_RUN_EXTRA_ARGS ]]; then
    # Add inherited extra docker options
    eval "CIJW_CI_DOCKER_EXTRA_ARGS=$DOCKER_RUN_EXTRA_ARGS"
    for op in "${CIJW_CI_DOCKER_EXTRA_ARGS[@]}"; do
        if [ "$op" == "--network" ]; then
            network_op="true"
        fi
        CIJW_DOCKER_JOB_ARGS+=("$op")
    done
fi

trap cijw_exit_hook EXIT SIGINT SIGTERM
CIJW_DIND_DIR="${CIJW_DIND_DIR:-"$CI_PROJECT_DIR/.dind"}"
if ! docker ps > /dev/null 2>&1 || [ "$CIJW_FORCE_MODE_DIND" == "true" ]; then
    if ! ip link add dummy0 type dummy >/dev/null 2>&1; then
        print_critical "Cannot start docker daemon in non privileged mode."
    else
        ip link delete dummy0 >/dev/null
    fi
    print_note "Starting docker service..."
    mkdir -p "$CIJW_DIND_DIR"
    DOCKER_TLS_CERTDIR="" dockerd --data-root "$CIJW_DIND_DIR" > /dev/null 2>&1 &
    remaining_try=5
    export DOCKER_HOST="unix:///var/run/docker.sock"
    until docker ps > /dev/null 2>&1; do
        if ! ((--remaining_try)); then
            print_critical "Unable to start docker daemon" \
                "Please check if this container is started in privileged mode enabled."
        fi
        sleep 1
    done
    CIJW_DIND_MODE_ACTIVE="true"
fi

# Clean previous one not stopped normally
if docker ps -a | grep -q "${HOSTNAME}-ci-job-wrapper$"; then
    print_note "Remove old ${HOSTNAME}-ci-job-wrapper docker"
    docker rm -f "${HOSTNAME}-ci-job-wrapper"
fi

cijw_process_image
cijw_process_network
cijw_process_docker_run_args
cijw_add_volumes
cijw_start_services

# Start...
print_info "Running the job $CI_JOB_NAME into the $CIJW_JOB_DOCKER_IMAGE docker container..."
docker run --rm --name "${HOSTNAME}-ci-job-wrapper" "${CIJW_DOCKER_COMMON_ARGS[@]}" "${CIJW_DOCKER_JOB_ARGS[@]}" \
    --label "$CIJW_DOCKER_PREFIX" -w $CI_PROJECT_DIR $CIJW_JOB_DOCKER_IMAGE "${CIJW_DOCKER_JOB_COMMANDS[@]}" "$@"
