# Changelog

## 4.3.0
* [ci-scripts/modules/dind] Wait for DIND up before run the build job
* [ci-scripts/jobs/build] Update TARGET_CI_REGISTRY_IMAGE_TAG
* Copyright update

## 4.2.0
* [ci-scripts] Update build job
* Add support of the job definition in the CIJW_JOB_CONFIG_YAML env variable
* [CI] Enable build on branches and push on tags only

## 4.1.0
* [entrypoint] Use safe_load python function
* [docker] Switch to docker v19.03.12
* [tests] Upgrade test jobs

## 4.0.0
* Rewrite YAML file process parts and add DIND mode support
* [tests] Add job-with-volumes and job-with-services

## 3.1.2
* Update project namespace

## 3.1.1
* [entrypoint] Improve the volume creation reliability
* [entrypoint] Check if a service is started
* [entrypoint] Fix environment propagation (remove additional quotes added on certain variables)
* [CI] Switch to docker version 18.03.1
* [CI] Add docker job tag
* [modules/dockerimg] Force CITBX_JOB_ACCESS_TO_HOST_DOCKER_SOCK_ENABLED option to true if dind service is not enabled
* [CI] Take CI-Toolbox out of this repository and add run-job-script

## 3.1.0
* [entrypoint] remove wait_before_run_job property
* [CI] add build scripts
* [ci-toolbox] Upgrade CI Toolbox to the version 4.0.0
* [CI] rebase on docker:17.12.0-ce
* [entrypoint] Fix volume creation when several wrappers try to create the same container
* [entrypoint] Add support of configuration by environnent variable CIJW_CONFIG_YAML and CIJW_CONFIG_JSON
* [entrypoint] Improve log message format

## 3.0.0)
* rebase docker image from ercom/docker:xx.xx.xx-ce-git
* [CI/toolbox] Update to the version 3.3.0
* [FIX] service with custom commands with default entrypoint

## 2.0.1
* [FIX] Eval YAML properties

## 2.0
* [README] Update the documentation
* Add CI tool box
* Add services support
* [Doc] Update the documentation

## 1.0.0
* Initial version
