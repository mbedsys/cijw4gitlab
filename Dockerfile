FROM registry.gitlab.com/mbedsys/docker:20.10.3-extra

RUN apk add --no-cache iptables
COPY docker-entrypoint.sh /usr/local/bin/docker-entrypoint.sh
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
